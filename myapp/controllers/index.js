// HOME
exports.index = function(req, res, next) {
	res.render('index', {title: 'Furry Friends'});
}

// MAP
exports.get_map = function(req, res, next) {
	res.render('map', {title: 'Furry Friends'});
}

// CATS
exports.get_cats = function(req, res, next) {
	res.render('cats', {title: 'Furry Friends'});
}

// DOGS
exports.get_dogs = function(req, res, next) {
	res.render('dogs', {title: 'Furry Friends'});
}

// DOG 1
exports.get_dog1 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog1;
		
		// render dog's page
		res.render('dog1', {loves: numloves, title: 'Furry Friends', breed: 'Yellow Lab', name: 'Cleo', likes: 'wiggling with excitement, making funny sounds, and stealing food'});
	})
}
exports.like_dog1 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog1++;
		var numloves = obj.dog1;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog1', {loves: numloves, title: 'Furry Friends', breed: 'Yellow Lab', name: 'Cleo', likes: 'wiggling with excitement, making funny sounds, and stealing food'});
	})	
}

// DOG 2
exports.get_dog2 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog2;
		
		// render dog's page
		res.render('dog2', {loves: numloves, title: 'Furry Friends', breed: 'Beagle mix', name: 'Daisy'});
	})
}
exports.like_dog2 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog2++;
		var numloves = obj.dog2;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog2', {loves: numloves, title: 'Furry Friends', breed: 'Beagle mix', name: 'Daisy'});
	})	
}

// DOG 3
exports.get_dog3 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog3;
		
		// render dog's page
		res.render('dog3', {loves: numloves, title: 'Furry Friends', breed: 'Havanese', name: 'Ellie'});
	})
}
exports.like_dog3 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog3++;
		var numloves = obj.dog3;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog3', {loves: numloves, title: 'Furry Friends', breed: 'Havanese', name: 'Ellie'});
	})	
}

// DOG 4
exports.get_dog4 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog4;
		
		// render dog's page
		res.render('dog4', {loves: numloves, title: 'Furry Friends', breed: 'Mini Goldendoodle', name: 'Finn'});
	})
}
exports.like_dog4 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog4++;
		var numloves = obj.dog4;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog4', {loves: numloves, title: 'Furry Friends', breed: 'Mini Goldendoodle', name: 'Finn'});
	})	
}

// DOG 5
exports.get_dog5 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog5;
		
		// render dog's page
		res.render('dog5', {loves: numloves, title: 'Furry Friends', breed: 'English Springer Spaniel', name: 'George'});
	})
}
exports.like_dog5 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog5++;
		var numloves = obj.dog5;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog5', {loves: numloves, title: 'Furry Friends', breed: 'English Springer Spaniel', name: 'George'});
	})	
}

// DOG 6
exports.get_dog6 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog6;
		
		// render dog's page
		res.render('dog6', {loves: numloves, title: 'Furry Friends', breed: 'Redtick Coonhound', name: 'Jed'});
	})
}
exports.like_dog6 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog6++;
		var numloves = obj.dog6;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog6', {loves: numloves, title: 'Furry Friends', breed: 'Redtick Coonhound', name: 'Jed'});
	})	
}

// DOG 7
exports.get_dog7 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog7;
		
		// render dog's page
		res.render('dog7', {loves: numloves, title: 'Furry Friends', breed: 'Black Lab and Border Collie mix', name: 'Lily'});
	})
}
exports.like_dog7 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog7++;
		var numloves = obj.dog7;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog7', {loves: numloves, title: 'Furry Friends', breed: 'Black Lab and Border Collie mix', name: 'Lily'});
	})	
}

// DOG 8
exports.get_dog8 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog8;
		
		// render dog's page
		res.render('dog8', {loves: numloves, title: 'Furry Friends', breed: 'Golden Retriever', name: 'Maggie'});
	})
}
exports.like_dog8 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog8++;
		var numloves = obj.dog8;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog8', {loves: numloves, title: 'Furry Friends', breed: 'Golden Retriever', name: 'Maggie'});
	})	
}

// DOG 9
exports.get_dog9 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog9;
		
		// render dog's page
		res.render('dog9', {loves: numloves, title: 'Furry Friends', breed: 'Goldendoodle', name: 'Mickey'});
	})
}
exports.like_dog9 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog9++;
		var numloves = obj.dog9;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog9', {loves: numloves, title: 'Furry Friends', breed: 'Goldendoodle', name: 'Mickey'});
	})	
}

// DOG 10
exports.get_dog10 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog10;
		
		// render dog's page
		res.render('dog10', {loves: numloves, title: 'Furry Friends', breed: 'Australian Shepherd', name: 'Mochi'});
	})
}
exports.like_dog10 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog10++;
		var numloves = obj.dog10;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog10', {loves: numloves, title: 'Furry Friends', breed: 'Australian Shepherd', name: 'Mochi'});
	})	
}

// DOG 11
exports.get_dog11 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog11;
		
		// render dog's page
		res.render('dog11', {loves: numloves, title: 'Furry Friends', breed: 'Goldendoodle', name: 'Murphy'});
	})
}
exports.like_dog11 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog11++;
		var numloves = obj.dog11;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog11', {loves: numloves, title: 'Furry Friends', breed: 'Goldendoodle', name: 'Murphy'});
	})	
}

// DOG 12
exports.get_dog12 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog12;
		
		// render dog's page
		res.render('dog12', {loves: numloves, title: 'Furry Friends', breed: 'Biewer Terrier', name: 'Peanut'});
	})
}
exports.like_dog12 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog12++;
		var numloves = obj.dog12;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog12', {loves: numloves, title: 'Furry Friends', breed: 'Biewer Terrier', name: 'Peanut'});
	})	
}

// DOG 13
exports.get_dog13 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog1;
		
		// render dog's page
		res.render('dog13', {loves: numloves, title: 'Furry Friends', breed: 'Bearded Collie', name: 'Remy'});
	})
}
exports.like_dog13 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog13++;
		var numloves = obj.dog13;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog13', {loves: numloves, title: 'Furry Friends', breed: 'Bearded Collie', name: 'Remy'});
	})	
}

// DOG 14
exports.get_dog14 = function(req, res, next) {
	const fs = require('fs') 

	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 

		// get the dog's like count
		var obj = JSON.parse(data);
		var numloves = obj.dog14;
		
		// render dog's page
		res.render('dog14', {loves: numloves, title: 'Furry Friends', breed: 'Yellow Lab', name: 'Westley'});
	})
}
exports.like_dog14 = function(req, res, next) {
	const fs = require('fs') 
  
	// read the JSON file
	fs.readFile('controllers/data.json', (err, data) => { 
		if (err) throw err; 
	
		// get the dog's count and increment it
		var obj = JSON.parse(data);
		obj.dog14++;
		var numloves = obj.dog14;

		// write to JSON file with updated count
		let newdata = JSON.stringify(obj);
		fs.writeFile('controllers/data.json', newdata, (err) => { 
			if (err) throw err; 
		}) 

		// render dog's page
		res.render('dog14', {loves: numloves, title: 'Furry Friends', breed: 'Yellow Lab', name: 'Westley'});
	})	
}