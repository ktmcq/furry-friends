var express = require('express');
var router = express.Router();

let index = require('../controllers/index');

/* GET home page */
router.get('/', index.index);
router.get('/home', index.index);

/* GET map page */
router.get('/map', index.get_map);

/* GET cats page */
router.get('/cats', index.get_cats);

/* GET and POST dogs pages */
router.get('/dogs', index.get_dogs);
router.get('/dog1', index.get_dog1);
router.post('/dog1', index.like_dog1);
router.get('/dog2', index.get_dog2);
router.post('/dog2', index.like_dog2);
router.get('/dog3', index.get_dog3);
router.post('/dog3', index.like_dog3);
router.get('/dog4', index.get_dog4);
router.post('/dog4', index.like_dog4);
router.get('/dog5', index.get_dog5);
router.post('/dog5', index.like_dog5);
router.get('/dog6', index.get_dog6);
router.post('/dog6', index.like_dog6);
router.get('/dog7', index.get_dog7);
router.post('/dog7', index.like_dog7);
router.get('/dog8', index.get_dog8);
router.post('/dog8', index.like_dog8);
router.get('/dog9', index.get_dog9);
router.post('/dog9', index.like_dog9);
router.get('/dog10', index.get_dog10);
router.post('/dog10', index.like_dog10);
router.get('/dog11', index.get_dog11);
router.post('/dog11', index.like_dog11);
router.get('/dog12', index.get_dog12);
router.post('/dog12', index.like_dog12);
router.get('/dog13', index.get_dog13);
router.post('/dog13', index.like_dog13);
router.get('/dog14', index.get_dog14);
router.post('/dog14', index.like_dog14);

module.exports = router;